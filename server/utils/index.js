const bunyan = require('bunyan');
const log = bunyan.createLogger({ name: 'Books Review' });

const shaffle = arr => arr
    .map(n => [Math.random(), n])
    .sort().map(n => n[1]);

const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const range = num => [...Array(num).keys()];

const createRatios = int => range(int).map(i =>
    range(4).map(int => getRandomInt(1, 10)));

module.exports = {
    shaffle,
    createRatios,
    log,
}