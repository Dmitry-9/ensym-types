const createRatios = require('../utils').createRatios;

const fakeChartData = {
    dimentions: ['Age', 'Readers amount'],
    labels: [
        'Baby Boomers (Roughly 50 to 70 years old)',
        'Generation X (Roughly 35 – 50 years old)',
        'Millennials, or Generation Y (18 – 34 years old)',
        'Generation Z, or iGeneration (Teens & younger)'
    ],
    ratios: createRatios(10),
}

module.exports = fakeChartData;
