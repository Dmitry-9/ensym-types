//#region 
const http = require('http');
const express = require('express');
const app = express();
const socketIo = require('socket.io');
const ev = require('../src/api/events');
const responses = require('./responses');
const envFile = require('dotenv').config().parsed;
const booksList = require('./dummyData');
const utils = require('./utils');
//#endregion

const { REACT_APP_PORT, TIMEOUT_MS } = envFile;
const server = http.createServer(app);
const io = socketIo(server);

const registerErrorHandler = socket => {
    process.on('uncaughtException', function (err) {
        socket.emit(ev.SERVER_ERROR, err);
    });
    registerErrorHandler.done = true;
}

io.on(ev.CONNECTION, socket => {
    if (!registerErrorHandler.done)
        registerErrorHandler(socket);
    
    utils.log.info('New client connected');
    
    socket.on(ev.API_REQUEST_BOOKS_LIST, () => {
        socket.emit(
            ev.API_RESPOND_BOOKS_LIST,
            utils.shaffle(booksList)
        );
        utils.log.info(ev.API_RESPOND_BOOKS_LIST);
    });

    socket.on(ev.API_REQUEST_CHART_DATA, ({ title }) => {
        utils.log.info(ev.API_RESPOND_CHART_DATA, title);

        setTimeout(() => {
            responses.chartData({title, socket});
        }, TIMEOUT_MS);
        
    });

    socket.on(ev.ERROR_BOUNDRY_HIT, ({error, info}) => {
        utils.log.info(ev.ERROR_BOUNDRY_HIT, error, info);
    });
    
    socket.on(ev.DISCONNECT, () => {
        utils.log.info('Client disconnected');
    });
});   

server.listen(
    REACT_APP_PORT,
    () => utils.log.info(`Listening on port ${REACT_APP_PORT}`)
);
