const ev = require('../../src/api/events');
const booksList = require('../dummyData');
const chartData = require('../dummyData/chartData');
const log = require('../utils').log;

const responseChartData = ({title, socket}) => {
    const index = booksList.findIndex(
        book => book.title === title
    );
    chartData.ratio = chartData.ratios[index];
    const respObj = {...chartData, title };
    delete respObj.ratios;

    socket.emit(
        ev.API_RESPOND_CHART_DATA,
        respObj,
    );
    log.info(ev.API_RESPOND_CHART_DATA, respObj);
}
module.exports = {
    chartData: responseChartData,
}