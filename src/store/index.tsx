import tabsReducer from '../reducers/tabsReducer';
import gridReducer from '../reducers/gridReducer';
import singleBookReducer from '../reducers/singleBookReducer';
import chartReducer from '../reducers/chartReducer';
import { AppState } from '../reducers/AppInterface';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

const rootReducer = combineReducers<AppState>({
    tabsContent: singleBookReducer,
    chartData: chartReducer,
    tabs: tabsReducer,
    tileData: gridReducer,
});

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));
export default store;
