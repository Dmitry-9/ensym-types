import { TileGrid, TileObject } from '../interfaces/gridContent';
import { ChartObject } from '../interfaces/chartContent';
const SET_HIGHLIGHTED_TAB = 'SET_HIGHLIGHTED_TAB';
const SET_VISIBLE_TAB = 'SET_VISIBLE_TAB';
const POPULATE_GRID = 'POPULATE_GRID';
const POPULATE_CHART = 'POPULATE_CHART';
const SET_TABS_CONTENT = 'SET_TABS_CONTENT';

export interface PopulateGrid {
    type: string;
    data: TileGrid;
}

export interface PopulateChart {
    type: string;
    data: ChartObject;
}

export interface SetHighlightedTab {
    type: string;
    id: number;
}

interface SetVisibleTab {
    type: string;
    id: number;
}

export interface SetTabsContent {
    type: string;
    singleBook: TileObject;
}

export type TabsActionTypes = SetHighlightedTab | SetVisibleTab


export default {
    SET_HIGHLIGHTED_TAB,
    SET_VISIBLE_TAB,
    POPULATE_GRID,
    POPULATE_CHART,
    SET_TABS_CONTENT,
};