import TYPES, {
  SetHighlightedTab,
  PopulateGrid,
  PopulateChart,
  SetTabsContent
} from './types';
import { TileGrid, TileObject } from '../interfaces/gridContent';
import { ChartObjectPopulated } from '../interfaces/chartContent';

export function setHighlightedTab(id: number): SetHighlightedTab {
  return {
    type: TYPES.SET_HIGHLIGHTED_TAB,
    id,
  }
}

export function setVisibleTab(id: number): SetHighlightedTab {
    return {
      type: TYPES.SET_VISIBLE_TAB,
      id,
    }
}

export function populateGrid(data: TileGrid): PopulateGrid {
  return {
    type: TYPES.POPULATE_GRID,
    data,
  }
}

export function populateChart(data: ChartObjectPopulated): PopulateChart {
  return {
    type: TYPES.POPULATE_CHART,
    data,
  }
}

export function setTabsContent(singleBook: TileObject): SetTabsContent {
  return {
    type: TYPES.SET_TABS_CONTENT,
    singleBook,
  }
}
