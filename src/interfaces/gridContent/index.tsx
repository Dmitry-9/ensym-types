import React from 'react';

export interface TileObject {
    image: string;
    title: string;
    summary: string;
    author: string;
}

export type TileGrid = TileObject[] | [];