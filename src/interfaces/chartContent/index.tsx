export type Ratio = [number, number, number, number];
type BabyBoomers = 'Baby Boomers (Roughly 50 to 70 years old)';
type GenerationX = 'Generation X (Roughly 35 – 50 years old)';
type Millennials = 'Millennials, or Generation Y (18 – 34 years old)';
type GenerationZ = 'Generation Z, or iGeneration (Teens & younger)';

export interface ChartObjectPopulated {
    dimentions: ['Age', 'Readers amount'],
    labels: [
        BabyBoomers,
        GenerationX,
        Millennials,
        GenerationZ,
    ],
    ratio: Ratio,
    title: string;
}

export type ChartRenderData = [
    BabyBoomers |
    GenerationX |
    Millennials |
    GenerationZ, number
][]


export type ChartObject = ChartObjectPopulated | {}
