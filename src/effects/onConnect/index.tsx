import ReactDOM from 'react-dom';
import React from 'react';
import { requestBooksList } from '../../api/requests'
import { useEffect } from 'react';
import { TileGrid } from '../../interfaces/gridContent';
import { ChartObjectPopulated } from '../../interfaces/chartContent';
import  { populateGrid, populateChart } from '../../actions';
import { PopulateGrid, PopulateChart } from '../../actions/types';
import store from '../../store';
import { socket } from '../../api/socket';
import '../../index.scss';
const ev = require('../../api/events');

socket.on(ev.API_RESPOND_BOOKS_LIST, (data: TileGrid) => {
    store.dispatch<PopulateGrid>(populateGrid(data));
});

socket.on(ev.API_RESPOND_CHART_DATA, (data: ChartObjectPopulated) => {
    store.dispatch<PopulateChart>(populateChart(data));
});

socket.on(ev.SERVER_ERROR, function(err: string) {
    ReactDOM.render(
        <div className={'error-message'}></div>,
        document.getElementById('root')
    );
});

const useOnConnectEffect = () => {
    useEffect(() => {
        requestBooksList()
        
        return () => console.log('Tear Down');
    }, []);
}

export default useOnConnectEffect;