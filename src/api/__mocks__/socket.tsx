const events = require('../../api/events');
import { TileGrid } from '../../interfaces/gridContent';
import  { populateGrid } from '../../actions';
import { PopulateGrid } from '../../actions/types';
import store from '../../store';

const mockedBooks: TileGrid = [
    {
        "image": "https://images-na.ssl-images-amazon.com/images/I/51C8Tg0TCaL._SX322_BO1,204,203,200_.jpg",
        "title": "The Book Thief",
        "summary": "Liesel Meminger is a foster girl living outside of Munich, who scratches out a meager existence for herself by stealing when she encounters something she can’t resist–books. With the help of her accordion-playing foster father, she learns to read and shares her stolen books with her neighbors during bombing raids as well as with the Jewish man hidden in her basement....",
        "author":"Alfred A. Knopf"
    },
    {"image":"https://images-na.ssl-images-amazon.com/images/I/61VxEKq8B1L._SX365_BO1,204,203,200_.jpg","title":"The Hobbit","summary":"Bilbo Baggins is a hobbit who enjoys a comfortable, unambitious life, rarely traveling any farther than his pantry or cellar. But his contentment is disturbed when the wizard Gandalf and a company of dwarves arrive on his doorstep one day to whisk him away on an adventure. They have launched a plot to raid the treasure hoard guarded by Smaug the Magnificent, a large and very dangerous dragon. Bilbo reluctantly joins their quest, unaware that on his journey to the Lonely Mountain he will encounte...","author":"Houghton Mifflin Harcourt"},{"image":"https://images-na.ssl-images-amazon.com/images/I/511Dl74cE9L._SX328_BO1,204,203,200_.jpg","title":"The Great Alone","summary":"Ernt Allbright, a former POW, comes home from the Vietnam war a changed and volatile man. When he loses yet another job, he makes an impulsive decision: he will move his family north, to Alaska, where they will live off the grid in America’s last true frontier...","author":"St. Martin's Press"},{"image":"https://images-na.ssl-images-amazon.com/images/I/51n97vsmHlL._SX307_BO1,204,203,200_.jpg","title":"Romeo and Juliet","summary":"In Romeo and Juliet, Shakespeare creates a violent world, in which two young people fall in love. It is not simply that their families disapprove; the Montagues and the Capulets are engaged in a blood feud. In this death-filled setting, the movement from love at first sight to the lovers’ final union in death seems almost inevitable. And yet, this play set in an extraordinary world has become the quintessential story of young love. In part because of its exquisite language, it is easy to respon...","author":"Simon & Schuster"},{"image":"https://images-na.ssl-images-amazon.com/images/I/41qI9quGIdL._SX324_BO1,204,203,200_.jpg","title":"Farenheight 451","summary":"Ray Bradbury’s internationally acclaimed novel Fahrenheit 451 is a masterwork of twentieth-century literature set in a bleak, dystopian future....","author":"Simon & Schuster"},{"image":"https://images-na.ssl-images-amazon.com/images/I/51r6XIPWmoL._SX331_BO1,204,203,200_.jpg","title":"The Lord of the Rings","summary":"In ancient times the Rings of Power were crafted by the Elven-smiths, and Sauron, the Dark Lord, forged the One Ring, filling it with his own power so that he could rule all others. But the One Ring was taken from him, and though he sought it throughout Middle-earth, it remained lost to him. After many ages it fell by chance into the hands of the hobbit Bilbo Baggins....","author":"Houghton Mifflin Harcourt"},{"image":"https://images-na.ssl-images-amazon.com/images/I/5128ATd9dSL._SX418_BO1,204,203,200_.jpg","title":"Harry Potter and the Deathly Hallows","summary":"A spectacular finish to a phenomenal series, Harry Potter and the Deathly Hallows is a bittersweet read for fans. The journey is hard, filled with events both tragic and triumphant, the battlefield littered with the bodies of the dearest and despised, but the final chapter is as brilliant and blinding as a phoenix's flame, and fans and skeptics alike will emerge from the confines of the story with full but heavy hearts, giddy and grateful for the experience....","author":"Scholastic"},{"image":"https://images-na.ssl-images-amazon.com/images/I/31wXW-2LRhL._SX292_BO1,204,203,200_.jpg","title":"Animal Farm","summary":"A farm is taken over by its overworked, mistreated animals. With flaming idealism and stirring slogans, they set out to create a paradise of progress, justice, and equality. Thus the stage is set for one of the most telling satiric fables ever penned—a razor-edged fairy tale for grown-ups that records the evolution from revolution against tyranny to a totalitarianism just as terrible....","author":"Signet Classics"},{"image":"https://images-na.ssl-images-amazon.com/images/I/51M-Bp5PcPL._SX338_BO1,204,203,200_.jpg","title":"Holes","summary":"Stanley Yelnats is under a curse. A curse that began with his no-good-dirty-rotten-pig-stealing-great-great-grandfather and has since followed generations of Yelnatses. Now Stanley has been unjustly sent to a boys’ detention center, Camp Green Lake, where the boys build character by spending all day, every day digging holes exactly five feet wide and five feet deep. There is no lake at Camp Green Lake. But there are an awful lot of holes....","author":"Yearling"},{"image":"https://images-na.ssl-images-amazon.com/images/I/51erHMLhIzL._SX334_BO1,204,203,200_.jpg","title":"The Lion, the Witch, and the Wardrobe","summary":"Four adventurous siblings—Peter, Susan, Edmund, and Lucy Pevensie—step through a wardrobe door and into the land of Narnia, a land frozen in eternal winter and enslaved by the power of the White Witch. But when almost all hope is lost, the return of the Great Lion, Aslan, signals a great change . . . and a great sacrifice....","author":"HarperCollins"}
];

const emitEvents = [
    events.CONNECTION,
    events.API_REQUEST_BOOKS_LIST,
    events.API_REQUEST_CHART_DATA,
    events.ERROR_BOUNDRY_HIT,
];

export const socket = {
    emit: (ev: string) => {
        if (!emitEvents.includes(ev))
            throw Error(`Unexpected EVENT emited: ${ev}`);
    },
    on: (ev: string, cb: (d: TileGrid) => void) => {
        if (ev === events.API_RESPOND_BOOKS_LIST)
            store.dispatch<PopulateGrid>(populateGrid(mockedBooks));
    },
};