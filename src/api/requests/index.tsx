import { RequestChart } from '../../interfaces';
import ev from '../events';
import { socket } from '../socket';

export const requestChartRelevantTo = (payload: RequestChart) => {
    socket.emit(ev.API_REQUEST_CHART_DATA, payload);
}

export const requestBooksList = () => {
    socket.emit(ev.API_REQUEST_BOOKS_LIST);
}