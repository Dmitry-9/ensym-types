import socketIOClient from 'socket.io-client';
const { REACT_APP_HOST, REACT_APP_PORT } = process.env;

export const socket: SocketIOClient.Socket = socketIOClient(
    `${REACT_APP_HOST}:${REACT_APP_PORT}`
);