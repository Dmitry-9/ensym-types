import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './components/App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import store from './store';
import ErrorBoundary from './components/ErrorBoundary';
import { ThemeProvider } from '@material-ui/styles';
import theme from './theme';

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <ErrorBoundary render={() => (
                <div className={'error-message'}/>
            )}
        >
            <Provider store={store}>
                <App/>
            </Provider>
        </ErrorBoundary>
    </ThemeProvider>,
    document.getElementById('root')
);

serviceWorker.unregister();
