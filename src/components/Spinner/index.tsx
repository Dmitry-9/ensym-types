import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import './styles.scss';

export const Spinner = () => {

  return (
    <div>
      <CircularProgress size={100} className={'spinner_loader'} />
    </div>
  );
}

export default Spinner;