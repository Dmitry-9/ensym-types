import React from 'react';
import { Spinner } from '.';
import dep from '../../setupTests';
import { ShallowWrapper } from 'enzyme';

const shallowWrapper: ShallowWrapper = dep.shallow(
    <Spinner />
);

describe('Spinner renders without errors', () => {
    it('renders Spinner correctly', () => {
        dep.expect(
            shallowWrapper.find('.spinner_loader').length        )
            .to.be.equal(1);
    });
});
