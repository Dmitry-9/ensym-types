import React from 'react';
import Button from '@material-ui/core/Button';
import './styles.scss';

export type TabButtonProps = {
    isSelected: boolean;
    onClick: () => void;
    label: string;
};

export const TabButton = ({isSelected, onClick, label}: TabButtonProps) => {
    
    return (
        <Button onClick={onClick} 
            variant="contained"
            className={'buttons_spacing'}
            color={isSelected ? 'primary' : 'secondary'}
        >
            {label}
        </Button>
    );
}

export default TabButton;
