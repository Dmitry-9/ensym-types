import React from 'react';
import { TabButton, TabButtonProps } from '.';
import dep from '../../setupTests';
import { ShallowWrapper } from 'enzyme';

const mockedProps: TabButtonProps = {
    isSelected: true,
    onClick: () => {},
    label: 'Summary',
}

const shallowWrapper: ShallowWrapper = dep.shallow(
    <TabButton {...mockedProps}/>
);

describe('TabButton renders without errors', () => {
    const btn = shallowWrapper.find('.buttons_spacing');

    it('renders TabButton correctly', () => {
        dep.expect(btn.length).to.be.equal(1);
    });
    it('TabButton child gets correct color prop', () => {
        dep.expect(btn.prop('color')).to.be.equal('primary');
    });
});
