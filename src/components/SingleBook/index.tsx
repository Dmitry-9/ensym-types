//#region 
import React from 'react';
import { TileObject } from '../../interfaces/gridContent'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import './styles.scss';
//#endregion

export interface SingleBookProps {
    tile: TileObject;
    pickBook: () => void;
    children?: React.ReactNode;
}

export default function SingleBook({pickBook, tile}: SingleBookProps) {
    
    return (
        <div className="singleBook">
            <Card
                onClick={e => {
                    e.preventDefault();
                    pickBook();
                }}
            >
                <CardActionArea>
                    <CardMedia
                        className={'media_singlebook'}
                        image={tile.image}
                        title={tile.title}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {tile.title}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {tile.summary}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        </div>
    );
}