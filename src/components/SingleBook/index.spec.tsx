import React from 'react';
import { Provider } from 'react-redux';
import { AppState } from '../../reducers/AppInterface';
import store from '../../store';
import SingleBook, { SingleBookProps } from '.';
import dep from '../../setupTests';
import { ReactWrapper } from 'enzyme';
import sinon from 'sinon';
import App from '../App';

const testTitle = 'The Lord of the Rings';
const testSelector = `div[title="${testTitle}"].media_singlebook`;
const imageUrl = `https://images-na.ssl-images-amazon.com/images/I/51r6XIPWmoL._SX331_BO1,204,203,200_.jpg`;

const mockedBook = {
    "image": imageUrl,
    "title": testTitle,
    "summary": "In ancient times the Rings of Power were crafted by the Elven-smiths, and Sauron, the ...",
    "author": "Foo",
};

const mockedProps: SingleBookProps = {
    tile:  mockedBook,
    pickBook: dep.sinon.spy(),
}
const wrapper: ReactWrapper<SingleBookProps> = dep.mount(
    <SingleBook {...mockedProps} />
);

const app: ReactWrapper<AppState> = dep.mount(
    <Provider store={store}><App/></Provider>
);

jest.mock('../../api/socket');

describe('SingleBook renders without errors', () => {
    it('renders correct title from props', () => {
        dep.expect(
            wrapper.find(testSelector).length
        ).to.equal(1);

    });
    it('should call pickBook', () => {
        wrapper.find(testSelector).simulate('click');
        
        const spy: unknown = mockedProps.pickBook;
        dep.assert((spy as sinon.SinonSpy).calledOnce);
    });
    it('should use book image for the background', () => {
        app.find(testSelector).simulate('click');

        app.update();
        // possibly access property of [undefined] -> should break test
        // @ts-ignore
        const cssVal = app
            .find('.background_image')
            .prop('style')
            .backgroundImage;
        
        dep.assert(cssVal === `url(${imageUrl})`);
    });
});
