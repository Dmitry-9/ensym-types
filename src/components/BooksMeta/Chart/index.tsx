//#region 
import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../reducers/AppInterface';
import { ChartObject, ChartObjectPopulated } from '../../../interfaces/chartContent';
import { populateChart } from '../../../actions';
import { PopulateChart } from '../../../actions/types';
import ChartPaint from './ChartPaint';
import Spinner from '../../Spinner';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import './style.scss';
import { ChartState } from '../../../reducers/chartReducer/ChartStateInterface';
//#endregion

export type ChartDataProps = {
    chartData: ChartObject;
    title: string;
    children?: React.ReactNode;
}

export const CHART = ({chartData, title}: ChartDataProps): JSX.Element => {
    return  (
        <Card className={'card_booksmeta'}>
            <CardContent >
                <Typography gutterBottom variant="h5" component="h2">
                    Readers demography
                </Typography>
                    {
                        isReady(chartData, title)
                        ? (
                            <ChartPaint 
                            chartData={chartData as ChartObjectPopulated}
                        />)
                        : <Spinner />
                    }
            </CardContent>
        </Card>
    );
}

function isReady(chartData: ChartObject, title: string) : boolean {
    if (!Object.keys(chartData).length)
        return false;
    
    const chartDataPopulated = (chartData as ChartObjectPopulated);
    if (typeof chartDataPopulated.title !== 'string') {
        throw new Error(`[chartData.title] should be a type of STRING,
            but ${typeof chartDataPopulated.title} given`);
        
    }
    const isTitleRelevant = chartDataPopulated.title === title;

    return isTitleRelevant;
}

const mapStateToProps = (state: AppState) : ChartState => ({
    chartData: state.chartData,
    title: state.tabsContent.title,
});

const mapDispatchToProps = (dispatch: Dispatch<PopulateChart>) => {
    return {
        passTileToTabs: (data: ChartObjectPopulated) => {
            dispatch(populateChart(data));
        },
    } 
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(CHART);
