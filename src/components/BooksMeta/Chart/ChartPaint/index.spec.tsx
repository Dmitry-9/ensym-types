import React from 'react';
import { ChartObjectPopulated } from '../../../../interfaces/chartContent'
import { ChartPaint, ChartPaintProps } from '.';
import dep from '../../../../setupTests';
import { ShallowWrapper } from 'enzyme';

const chartData: ChartObjectPopulated = {
    dimentions: ['Age', 'Readers amount'],
    labels:[
        'Baby Boomers (Roughly 50 to 70 years old)',
        'Generation X (Roughly 35 – 50 years old)',
        'Millennials, or Generation Y (18 – 34 years old)',
        'Generation Z, or iGeneration (Teens & younger)' ],
    ratio: [ 7, 3, 2, 4 ],
    title: 'The Hobbit',
}

const mockedProps: ChartPaintProps = {
    chartData,
}

const shallowWrapper: ShallowWrapper<ChartPaintProps> = dep.shallow(
    <ChartPaint {...mockedProps} />
);

describe('CharttPaint renders without errors', () => {
    it('renders CharttPaint correctly', () => {
        dep.expect(shallowWrapper.find('.chart_container').length)
            .to.equal(1);
    });
});
