//#region 
import React from 'react';
import { Chart } from 'react-google-charts';
import { Ratio, ChartRenderData } from '../../../../interfaces/chartContent';
import { ChartObjectPopulated } from '../../../../interfaces/chartContent';
import '../style.scss';
//#endregion

export type ChartPaintProps = {
    chartData: ChartObjectPopulated;
}

export const ChartPaint = ({chartData}: ChartPaintProps)  => {
    const relevantRatios: Ratio = chartData.ratio;

    const data: ChartRenderData = chartData.labels.map((label, index) => {
        return [label, relevantRatios[index]];
    });
    return (
        <Chart
            className={'chart_container'}
            width={'600px'}
            height={'220px'}
            chartType="PieChart"
            data={[
                chartData.dimentions,
                ...data,
            ]}
            options={{
                title: '',
                is3D: true,
            }}
        />
    );
}

export default ChartPaint;
