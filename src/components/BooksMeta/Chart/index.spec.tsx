import React from 'react';
import { ChartObject } from '../../../interfaces/chartContent'
import { CHART, ChartDataProps } from '.';
import dep from '../../../setupTests';
import { ShallowWrapper } from 'enzyme';

const chartData: ChartObject = {
    dimentions: ['Age', 'Readers amount'],
    labels:[
        'Baby Boomers (Roughly 50 to 70 years old)',
        'Generation X (Roughly 35 – 50 years old)',
        'Millennials, or Generation Y (18 – 34 years old)',
        'Generation Z, or iGeneration (Teens & younger)' ],
    ratio: [ 7, 3, 2, 4 ],
    title: 'The Hobbit',
}

const mockedProps: ChartDataProps = {
    chartData,
    title: 'The Hobbit',
}

const shallowWrapper: ShallowWrapper<ChartDataProps> = dep.shallow(
    <CHART {...mockedProps} />
);

describe('CHART renders without errors', () => {
    it('renders CHART header correctly', () => {
        dep.expect(shallowWrapper.text() === 'Readers demography<ChartPaint />')
        .to.be.true;
    });
});
