//#region 
import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import '../styles.scss';
//#endregion

export type ReviewProps = {
    id: number;
    children?: React.ReactNode;
};

export const reviewPlaceholder = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.  The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters ...';

export const Review = (props: ReviewProps) => {
    return (
        <Card className={'card_booksmeta'}>
            <CardContent>
                <Typography variant="body2" color="textSecondary" component="p">
                    {reviewPlaceholder}
                </Typography>
            </CardContent>
        </Card>
    );
}

export default Review;