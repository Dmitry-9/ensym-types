import React from 'react';
import { Review, reviewPlaceholder, ReviewProps } from '.';
import dep from '../../../setupTests';
import { ShallowWrapper } from 'enzyme';

const mockedProps: ReviewProps = {
    id: 2,
}
const shallowWrapper: ShallowWrapper = dep.shallow(
    <Review {...mockedProps} />
);

describe('Review renders without errors', () => {
    it('renders Review correctly', () => {
        dep.expect(
            shallowWrapper.text() === reviewPlaceholder
        )
            .to.be.true;
    });
});
