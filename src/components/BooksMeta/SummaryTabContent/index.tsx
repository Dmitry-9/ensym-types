//#region 
import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { TabsContent } from '../../../reducers/singleBookReducer/TabsContentInterface';
import '../styles.scss';
//#endregion

const Summary = ({tabsContent}: TabsContent) => {
    return (
        <Card className={'card_booksmeta'}>
            <CardContent>
                <Typography gutterBottom
                    variant="h5"
                    component="h2"
                >
                    {tabsContent.title}
                </Typography>
                <Typography 
                    variant="body2"
                    color="textSecondary"
                    component="p"
                >
                    {tabsContent.summary}
                </Typography>
            </CardContent>
        </Card>
    );
}        

export default Summary;