import React from 'react';
import { Provider } from 'react-redux';
import { AppState } from '../../../reducers/AppInterface';
import store from '../../../store';
import dep from '../../../setupTests';
import { ReactWrapper } from 'enzyme';
import Summary from '.';
import { TabsContent } from '../../../reducers/singleBookReducer/TabsContentInterface';
import App from '../../App';

const testTitle = 'The Lord of the Rings';
const testSelector = `div[title="${testTitle}"].media_singlebook`;
const testSummary = "In ancient times the Rings of Power were crafted by the Elven-smiths, and Sauron, the ...";

const mockedProps: TabsContent = {
    tabsContent: {
        "image": 'imageurl',
        "title": testTitle,
        "summary": testSummary,
        "author": "Foo",
    }
};

const app: ReactWrapper<AppState> = dep.mount(
    <Provider store={store}><App/></Provider>
);

const wrapper: ReactWrapper<TabsContent> = dep.mount(
    <Summary {...mockedProps} />
);

jest.mock('../../../api/socket');

describe('SummaryTab renders without errors', () => {
    it('should render tabsContent component correctly', () => {
        dep.assert(testTitle === wrapper.find('h2').text());
        dep.assert(testSummary === wrapper.find('p').text());
    });
    it('should update tabsContent state correctly', () => {
        const getTitleState = () => {
            return store.getState().tabsContent.title;
        }
    
        dep.assert(getTitleState() === 'title placeholder');
        app.find(testSelector).simulate('click');
        dep.assert(getTitleState() === testTitle);
    });
});