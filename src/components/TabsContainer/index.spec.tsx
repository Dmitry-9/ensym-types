import React from 'react';
import { TabsContainer, TabsContainerProps } from '.';
import { TileObject } from '../../interfaces/gridContent';
import dep from '../../setupTests';
import { ShallowWrapper } from 'enzyme';

const mockedTile: TileObject = {
    image: 'Hobbit image url',
    title: 'Hobbit',
    summary: 'Hobbit summary',
    author: 'Hobbit author', 
}

const mockedProps: TabsContainerProps = {
    tabsContent: mockedTile,
    toggle: () => {},
}

const shallowWrapper: ShallowWrapper = dep.shallow(
    <TabsContainer {...mockedProps}/>
);

describe('TabsContainer renders without errors', () => {
    const btns = shallowWrapper.find('.background_fader');

    it('renders TabsContainer children correctly', () => {
        dep.expect(
            shallowWrapper.find('.background_fader').length
        ).to.be.equal(1);

        dep.expect(
            shallowWrapper.find('.fab_search_container').length
        ).to.be.equal(1);
    });
});
