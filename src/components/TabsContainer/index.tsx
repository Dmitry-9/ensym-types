import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Fab from '@material-ui/core/Fab';
import SearchIcon from '@material-ui/icons/Search';
import { TileObject } from '../../interfaces/gridContent';
import './styles.scss';

export type TabsContainerProps = {
    children?: React.ReactNode[],
    tabsContent: TileObject,
    toggle: () => void,
};

export const TabsContainer = ({
        tabsContent,
        toggle,
        children,
    }: TabsContainerProps) => {
    
    return (
        <>
            <div className={'background_image'}
                style={{backgroundImage: `url(${tabsContent.image})`}}
            >
            </div>
            <div className={'background_fader'}>
                <CssBaseline />
                <Container className={'tabs_wrapper'} 
                    maxWidth="sm">
                    {children}
                </Container>
            </div>
            <Fab 
                onClick={toggle}
                className={'fab_search_container'}
                aria-label="Search"
            >
                    <SearchIcon />
            </Fab>
        </>
    );
}
    
export default TabsContainer;
