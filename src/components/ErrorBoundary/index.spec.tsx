import React from 'react';
import ErrorBoundary from '.';
import dep from '../../setupTests';

jest.mock('../../api/socket');

const Child = () => {
    throw 'error';
}

const swallowErrors = (codeToRun: () => void) => {
    const error = console.error;
    console.error = () => {};
    codeToRun();
    console.error = error;
}

it('catches error and display err0r-message', () => {
    swallowErrors(() => {
        const wrapper = dep.mount(
            <ErrorBoundary render={() => (
                <div className={'error-message'}></div>
                )}
            >
                <Child />
            </ErrorBoundary>
        );
    
        dep.expect(
            wrapper.childAt(0).equals(<div className="error-message" />)
        ).to.equal(true);
    });
});