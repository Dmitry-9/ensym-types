import React from 'react';
import ev from '../../api/events';
import { socket } from '../../api/socket';

interface IState {
    hasError: boolean
}
interface IProps {
    render: any
    children: React.ReactNode
}

export default class ErrorBoundary extends React.Component <IProps, IState> {
    state = { hasError: false };
    
    static getDerivedStateFromError(error: any) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    componentDidCatch(error: Error | null, info: object) {
        socket.emit(ev.ERROR_BOUNDRY_HIT, {error, info});
    }
    
    render() {
        if (this.state.hasError) {
            return this.props.render();
        }
        
        return this.props.children; 
    }
}