import React from 'react';
import { Provider } from 'react-redux';
import App from '.';
import store from '../../store';
import dep from '../../setupTests';
import { ReactWrapper } from 'enzyme';
import { AppState } from '../../reducers/AppInterface';

jest.mock('../../api/socket');

const app: ReactWrapper<AppState> = dep.mount(
    <Provider store={store}><App/></Provider>
);

it('fetches books from api and renders them on mount', () => {
    // console.log(dep.mount(app).childAt(0).debug());

    dep.expect(
        app.find('BookList').length
    ).to.equal(1);

    dep.expect(
        app.find('.singleBook').length
    ).to.equal(10);

});
