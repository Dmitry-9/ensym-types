//#region 
import React, { useState } from 'react';
import { connect } from 'react-redux';
import BooksList from '../BooksList';
import Tabs from '../Tabs';
import Review from '../BooksMeta/Reviews';
import TabsContainer from '../TabsContainer';
import useOnConnectEffect from '../../effects/onConnect';
import './App.scss';
import { AppState, AppStateWithoutChartData } from '../../reducers/AppInterface';
import SummaryTabsContent from '../BooksMeta/SummaryTabContent';
import Chart from '../BooksMeta/Chart';
//#endregion

const App = ({tabs, tileData, tabsContent}: AppStateWithoutChartData) => {
    useOnConnectEffect();
    const [isListVisible, setListVisible] = useState(true);

    return isListVisible
        ? <BooksList 
            tileData={tileData} 
            toggle={() => setListVisible(false)}
        />
        : <TabsContainer tabsContent={tabsContent} toggle={() => setListVisible(true)}>
            <Tabs />
            <TabContent isVisible={tabs.visibleTab === 1}>
                <SummaryTabsContent tabsContent={tabsContent}/>
            </TabContent>
            <TabContent isVisible={tabs.visibleTab === 2}>
                <Chart />
            </TabContent>
            <TabContent isVisible={tabs.visibleTab === 3}>
                {[1,2,3].map(reviewId => (
                    <Review
                        key={reviewId}
                        id={reviewId}
                    />
                ))}
            </TabContent>
        </TabsContainer>;
}
        
type Props = {
    isVisible: boolean;
    children: React.ReactNode;
};

function TabContent({isVisible, children} : Props) {
    if (!isVisible) {
        return <div hidden={true}>{children}</div>
    } else {
        return <div>{children}</div>
    }
}

const mapStateToProps = (state: AppState) => {
    return ({
        tabs: state.tabs,
        tabsContent: state.tabsContent,
        tileData: state.tileData,
    });
}

export default connect(
    mapStateToProps,
    null,
)(App);