import React from 'react';
import { Tabs, TabsProps } from '.';
import {setVisibleTab, setHighlightedTab} from '../../actions/index'
import { TabsSTate } from '../../reducers/tabsReducer/TabsStateInterface';
import { AppState } from '../../reducers/AppInterface';
import dep from '../../setupTests';
import store from '../../store';
import { ShallowWrapper, ReactWrapper } from 'enzyme';

const mockedTabs: TabsSTate = {
    highlightedTab: 1,
    visibleTab: 1,
    labels: ['Summary', 'Stats', 'Reviews'] 
}

const propsWithSpy: TabsProps = {
    tabs: mockedTabs,
    change: dep.sinon.spy(),
}

const shallowWrapper: ShallowWrapper = dep.shallow(
    <Tabs {...propsWithSpy}/>
);
const wrapperWithSpy: ReactWrapper = dep.mount(
    <Tabs {...propsWithSpy}/>
);

const testChangeFn = (id: number) => {
    store.dispatch(setHighlightedTab(id));
    store.dispatch(setVisibleTab(id));
}
const mockedProps: TabsProps = {
    tabs: mockedTabs,
    change: testChangeFn,
}
const wrapper: ReactWrapper = dep.mount(
    <Tabs {...mockedProps}/>
);
const allLabels = store.getState().tabs.labels;

const getTabsState = () => {
    const state: AppState = store.getState();
    return {
        visible: state.tabs.visibleTab,
        highlighted: state.tabs.highlightedTab,
    }
}


const getClickedLabel = (index: number) : String => {
    const btn = wrapper
        .find('TabButton')
        .at(index)
        
    btn.simulate('click');

    return btn
        .find('button > span')
        .text();
}
        
describe('Tabs renders without errors', () => {
    const btns = shallowWrapper.find('TabButton');

    it('renders Tabs children correctly', () => {
        dep.expect(btns.length).to.be.equal(3);
    });
    it('Tabs child gets correct color prop', () => {
        dep.expect(btns.at(0).prop('label') === 'Summary').to.be.true;
    });
    it('should call [change] with correct args', () => {
        const spy: unknown = propsWithSpy.change;
        [1, 2, 3].map((arg, index) => {
            wrapperWithSpy.find('TabButton').at(index).simulate('click');
            dep.assert(
                (spy as sinon.SinonSpy).withArgs(arg).calledOnce
            );
        });
    });
    it('should change [stat.tabs.highlightedTab] correctly', () => {
        const explicit: object[] = [];
        const shoulBe = JSON.stringify([
            { highlighted: 0, activeLabel: 'Summary' },
            { highlighted: 1, activeLabel: 'Stats' },
            { highlighted: 2, activeLabel: 'Reviews' },
        ]);
        [0, 1, 2].map(numOftab => {
            const activeLabel = getClickedLabel(numOftab);
            const { highlighted } = getTabsState();
    
            dep.assert(
                allLabels[highlighted - 1] === activeLabel
            );
                
            explicit.push({highlighted: highlighted - 1, activeLabel});

        });
        dep.assert(JSON.stringify(explicit) === shoulBe);
    });
    it('should change [stat.tabs.visibleTabsContent] correctly', () => {
        [0, 1, 2].map(numOftab => {
            const activeLabel = getClickedLabel(numOftab);
            const { visible } = getTabsState();
    
            dep.assert(
                allLabels[visible - 1] === activeLabel
            );
        });
    });
});
