import React from 'react';
import { Dispatch } from 'redux';
import {TabsActionTypes} from '../../actions/types'
import { connect } from 'react-redux';
import TabButton from '../TabButton';
import {setVisibleTab, setHighlightedTab} from '../../actions/index'
import {AppState} from '../../reducers/AppInterface';
import {TabsSTate} from '../../reducers/tabsReducer/TabsStateInterface';

export type TabsProps = {
    tabs: TabsSTate;
    change: (id: number) => void;
};

export const Tabs = ({tabs, change}: TabsProps) => {
    return (
        <div>
            {tabs.labels.map((label, index) => (
                <TabButton
                    key={label}
                    isSelected={(index + 1) === tabs.highlightedTab}
                    onClick={() => change(index + 1)}
                    label={label}
                />
            ))}
        </div>
    )
}

const mapStateToProps = (state: AppState) => ({
    tabs: state.tabs,
});

const mapDispatchToProps = (dispatch: Dispatch<TabsActionTypes>) => {
    return {
        change: (id: number) => {
            dispatch(setHighlightedTab(id));
            dispatch(setVisibleTab(id));
        },
    } 
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Tabs);