//#region 
import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../reducers/AppInterface';
import { TileObject } from '../../interfaces/gridContent'
import { setTabsContent } from '../../actions';
import { SetTabsContent } from '../../actions/types';
import { TabsContent } from '../../reducers/singleBookReducer/TabsContentInterface';
import SingleBook from '../SingleBook';
import GridList from '@material-ui/core/GridList';
import { requestChartRelevantTo } from '../../api/requests'
import './styles.scss';
//#endregion

export type ListContainerProps = {
    tileData: TileObject[];
    toggle: () => void;
    passTileToTabs: (tile: TileObject) => void;
}

export function BookList({tileData, toggle, passTileToTabs}: ListContainerProps) {    
    // throw new Error('way');

    return (
        <div className={'wraper_booklist'}>
            <GridList cellHeight={180} cols={4} className={'gridList_booklist'}>
                {tileData.map((tile: TileObject) => (
                    <SingleBook 
                        tile={tile}
                        pickBook={
                            () => {
                                requestChartRelevantTo({title: tile.title});
                                passTileToTabs(tile);
                                toggle();
                            }
                        } 
                        key={tile.image}
                    >
                    </SingleBook>
                ))}
            </GridList>
        </div>);
}

const mapStateToProps = (state: AppState) : TabsContent => ({
    tabsContent: state.tabsContent,
});

const mapDispatchToProps = (dispatch: Dispatch<SetTabsContent>) => {
    return {
        passTileToTabs: (tile: TileObject) => {
            dispatch(setTabsContent(tile));
        },
    } 
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(BookList);
