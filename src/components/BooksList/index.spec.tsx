import React from 'react';
import { setTabsContent } from '../../actions';
import { TileObject } from '../../interfaces/gridContent'
import { BookList, ListContainerProps } from '.';
import store from '../../store';
import dep from '../../setupTests';
import { ShallowWrapper } from 'enzyme';

jest.mock('../../api/socket');

const mockedTileData = [
    {
        "image": "https://images-na.ssl-images-amazon.com/images/I/51r6XIPWmoL._SX331_BO1,204,203,200_.jpg",
        "title": "The Lord of the Rings",
        "summary": "In ancient times the Rings of Power were crafted by the Elven-smiths, and Sauron, the ...",
        "author": "Foo",
    },
    {
        "image": "https://images-na.ssl-images-amazon.com/images/I/5128ATd9dSL._SX418_BO1,204,203,200_.jpg",
        "title": "Harry Potter and the Deathly Hallows",
        "summary": "A spectacular finish to a phenomenal series, Harry Potter and the Deathly Hallows is a bittersweet...",
        "author": "Scholastic",
    },
    {
        "image": "https://images-na.ssl-images-amazon.com/images/I/31wXW-2LRhL._SX292_BO1,204,203,200_.jpg",
        "title": "Animal Farm",
        "summary": "A farm is taken over by its overworked, mistreated animals. With flaming idealism and ...",
        "author": "Signet Classics"
    },
];

const mockedProps: ListContainerProps = {
    tileData: mockedTileData, 
    toggle: () => {},
    passTileToTabs: (tile: TileObject) => {
        store.dispatch(setTabsContent(tile));
    }
}
const wrapper = dep.mount(
    <BookList {...mockedProps} />
);

const shallowWrapper: ShallowWrapper<ListContainerProps> = dep.shallow(<BookList {...mockedProps} />);

describe('BookList renders without errors', () => {
    it('renders SingleBook components from props', () => {
        dep.expect(wrapper.find('.singleBook').length)
            .to.equal(3);
    });
    it('renders BooksList SingleBook-children correctly', () => {
        dep.expect(shallowWrapper
            .find('.gridList_booklist')
            .dive().find('SingleBook')
        ).to.have.lengthOf(3);
    });
});
