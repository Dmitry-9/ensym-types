import Enzyme, {shallow, render, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { createSerializer } from 'enzyme-to-json';
import sinon from 'sinon';
import chai from 'chai';

expect.addSnapshotSerializer(createSerializer({ mode: 'deep'}));

Enzyme.configure({ adapter: new Adapter() });

    global.shallow = shallow;
    global.render = render;
    global.mount = mount;
    global.sinon = sinon;
    global.chai = chai;

export default {
    shallow,
    render,
    mount,
    sinon,
    assert: chai.assert,
    expect: chai.expect,
    chai,
}