import { TabsSTate } from './tabsReducer/TabsStateInterface';
import { TileGrid, TileObject } from '../interfaces/gridContent';
import { ChartObject } from '../interfaces/chartContent';

export interface AppState  {
    tabs: TabsSTate;
    tileData: TileGrid;
    tabsContent: TileObject;
    chartData: ChartObject;
}

export interface AppStateWithoutChartData  {
    tabs: TabsSTate;
    tileData: TileGrid;
    tabsContent: TileObject;
}