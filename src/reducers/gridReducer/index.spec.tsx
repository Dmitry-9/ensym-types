import TYPES from '../../actions/types';
import gridReducer from '.';
import { TileObject } from '../../interfaces/gridContent';

const book: TileObject = {
    "image": "book img src",
    "title": "book tittle",
    "summary": "book summary", 
    "author": "book author"
}

describe('gridBook Reducer', () => {

    it('Should return default state', () => {
        const newState = gridReducer(undefined, {
            type: 'NOT_EXISTING_ACTION',
            data: []
        });
        expect(newState).toEqual([]);
    });

    it('Should return new state if receiving type', () => {

        const newState = gridReducer(undefined, {
            type: TYPES.POPULATE_GRID,
            data: [book],
        });
        expect(newState).toEqual([book]);

    });

});