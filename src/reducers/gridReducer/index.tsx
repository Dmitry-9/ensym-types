import TYPES, {
    PopulateGrid,
} from '../../actions/types';
import { TileGrid } from '../../interfaces/gridContent';

export function gridReducer(
    state: TileGrid = [],
    action: PopulateGrid,
): TileGrid {
    switch (action.type) {
        case TYPES.POPULATE_GRID:
            return action.data
        default:
            return state;
    }
}

export default gridReducer;