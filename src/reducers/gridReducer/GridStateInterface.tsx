import { TileGrid } from '../../interfaces/gridContent';
 
export interface GridSTate {
    tileData: TileGrid;
}