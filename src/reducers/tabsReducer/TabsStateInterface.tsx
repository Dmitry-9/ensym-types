type Labels = 'Summary' | 'Stats' | 'Reviews';
export interface TabsSTate {
    highlightedTab: number,
    visibleTab: number,
    labels: Labels[] 
}