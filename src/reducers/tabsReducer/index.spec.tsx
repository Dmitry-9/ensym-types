import TYPES, {TabsActionTypes} from '../../actions/types';
import tabsReducer, { initialState } from '.';

describe('tabs Reducer', () => {
    const action: TabsActionTypes = {
        type: 'NOT_EXISTING_ACTION',
        id: 1,
    }
    
    it('Should return default state', () => {
        const newState = tabsReducer(undefined, action );
        expect(newState).toEqual(initialState);
    });

    it('Should return new state if receiving type', () => {

        const newState = tabsReducer(undefined, {
            type: TYPES.SET_HIGHLIGHTED_TAB,
            id: 3
        });
        expect(newState)
            .toEqual({...initialState, highlightedTab: 3});
        
        const updatedState = tabsReducer(undefined, {
            type: TYPES.SET_VISIBLE_TAB,
            id: 2
        });
        expect(updatedState)
            .toEqual({...initialState, visibleTab: 2});
    });

});