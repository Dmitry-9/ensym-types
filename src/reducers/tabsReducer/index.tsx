import TYPES, {
    TabsActionTypes,
} from '../../actions/types';
import { TabsSTate } from './TabsStateInterface';

export const initialState: TabsSTate = {
    highlightedTab: 1,
    visibleTab: 1,
    labels: ['Summary', 'Stats', 'Reviews'],
}

export function tabsReducer(
    state = initialState,
    action: TabsActionTypes,
): TabsSTate {
    switch (action.type) {
        case TYPES.SET_HIGHLIGHTED_TAB:
            return {
                ...state,
                highlightedTab: action.id,
            }
        case TYPES.SET_VISIBLE_TAB:
            return {
                ...state,
                visibleTab: action.id,
            }
        default:
            return state;
    }
}

export default tabsReducer;