import TYPES from '../../actions/types';
import singleBookReducer from '.';
import { TileObject } from '../../interfaces/gridContent';

const placeholder: TileObject = {
    "image": "image placeholder",
    "title": "title placeholder",
    "summary": "summary placeholder", 
    "author": "author placeholder"
}

const book: TileObject = {
    "image": "book img src",
    "title": "book tittle",
    "summary": "book summary", 
    "author": "book author"
}

describe('singleBook Reducer', () => {

    it('Should return default state', () => {
        const newState = singleBookReducer(undefined, {
            type: 'NOT_EXISTING_ACTION',
            singleBook: book
        });
        expect(newState).toEqual(placeholder);
    });

    it('Should return new state if receiving type', () => {

        const newState = singleBookReducer(undefined, {
            type: TYPES.SET_TABS_CONTENT,
            singleBook: book
        });
        expect(newState).toEqual(book);

    });

});