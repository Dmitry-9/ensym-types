import { TileObject } from '../../interfaces/gridContent';

export interface TabsContent {
    tabsContent: TileObject;
}