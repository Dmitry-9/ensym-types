import TYPES, {
    SetTabsContent,
} from '../../actions/types';
import { TileObject } from '../../interfaces/gridContent';

const placeholder: TileObject = {
    "image": "image placeholder",
    "title": "title placeholder",
    "summary": "summary placeholder", 
    "author": "author placeholder"
}

export function singleBookReducer(
    state: TileObject = placeholder,
    action: SetTabsContent,
): TileObject {
    switch (action.type) {
        case TYPES.SET_TABS_CONTENT:
            return action.singleBook
        default:
            return state;
    }
}

export default singleBookReducer;