import { ChartObject } from '../../interfaces/chartContent';

export interface ChartState {
    chartData: ChartObject;
    title: string;
};
