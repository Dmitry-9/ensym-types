import TYPES, {
    PopulateChart,
} from '../../actions/types';
import { ChartObject } from '../../interfaces/chartContent';

export function chartReducer(
    state: ChartObject = {},
    action: PopulateChart,
): ChartObject {
    switch (action.type) {
        case TYPES.POPULATE_CHART:
            return action.data
        default:
            return state;
    }
}

export default chartReducer;