import TYPES from '../../actions/types';
import chartReducer from '.';
import { ChartObject } from '../../interfaces/chartContent';

const fakeChartData: ChartObject = {
    dimentions: ['Age', 'Readers amount'],
    labels: [
        'Baby Boomers (Roughly 50 to 70 years old)',
        'Generation X (Roughly 35 – 50 years old)',
        'Millennials, or Generation Y (18 – 34 years old)',
        'Generation Z, or iGeneration (Teens & younger)'
    ],
    ratios: [
        [4, 5, 1, 12],
        [14, 7, 3, 9],
        [6, 2, 8, 1],
    ],
}

describe('chart Reducer', () => {

    it('Should return default state', () => {
        const newState = chartReducer(undefined, {
            type: 'NOT_EXISTING_ACTION',
            data: {}
        });
        expect(newState).toEqual({});
    });

    it('Should return new state if receiving type', () => {

        const newState = chartReducer(undefined, {
            type: TYPES.POPULATE_CHART,
            data: fakeChartData,
        });
        expect(newState).toEqual(fakeChartData);

    });

});